<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/rules', 'HomeController@rules')->name('rules');

Route::resource('links', 'LinkController', ['only' => ['index', 'store', 'show', 'destroy']]);
Route::get('links/prolong/{link}', 'LinkController@prolong')->name('links.prolong');
Route::post('links/disable/{link}', 'LinkController@disable')->name('links.disable');

Route::resource('feedback', 'FeedbackController', ['only' => ['create', 'store']]);

Route::group(
    [
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'as' => 'admin.',
        'middleware' => ['auth', 'can:admin-role']
    ],
    function() {
        Route::get('/', 'HomeController@index')->name('index');
        Route::resource('domains', 'DomainController', ['only' => ['index', 'edit', 'update']]);
        Route::resource('banned-domains', 'BannedDomainController', ['except' => ['show']]);
        Route::resource('users', 'UserController', ['only' => ['index', 'show']]);
        Route::post('users/{user}/premium-add', 'UserController@premiumAdd')->name('users.premium-add');
        Route::resource('feedback', 'FeedbackController', ['only' => ['index', 'show', 'destroy']]);
    }
);

Route::get('/api', function() { abort(404); });
Route::get('/news', function() { abort(404); });
Route::get('/blog', function() { abort(404); });
Route::get('/articles', function() { abort(404); });

Route::get('/{key}', 'RedirectController@redirect')->name('redirect');

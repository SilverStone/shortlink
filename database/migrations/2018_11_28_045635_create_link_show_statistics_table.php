<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkShowStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_show_statistics', function (Blueprint $table) {
            $table->integer('link_id')->unsigned();
            $table->char('ip', 15)->nullable();
            $table->string('country_iso_code', 5)->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('platform')->nullable();
            $table->tinyInteger('device_type')->unsigned()->nullable();
            $table->string('browser')->nullable();
            $table->string('browser_version')->nullable();
            $table->timestamp('created_at')->index();

            $table->foreign('link_id')->references('id')->on('links')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('link_show_statistics', function (Blueprint $table) {
            $table->dropForeign(['link_id']);
        });

        Schema::dropIfExists('link_show_statistics');
    }
}

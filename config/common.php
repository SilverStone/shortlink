<?php

    return [
        'link_length_guest' => env('LINK_LENGTH_GUEST', 6),
        'link_length_registered' => env('LINK_LENGTH_REGISTERED', 5),
        'link_length_premium' => env('LINK_LENGTH_PREMIUM', 4),

        'link_lifetime_guest' => env('LINK_LIFETIME_GUEST', 30),
        'link_lifetime_registered' => env('LINK_LIFETIME_REGISTERED', 90),
        'link_lifetime_premium' => env('LINK_LIFETIME_PREMIUM', 180),

        'link_clear_disabled_after' => env('LINK_CLEAR_DISABLED_AFTER', 20),

        'link_prolong_period' => env('LINK_PROLONG_PERIOD', 60),

        'link_generate_iteration_limit' => env('LINK_GENERATE_ITERATION_LIMIT', 200),

        'link_symbols_string' => env('LINK_SYMBOLS_STRING', 'abcdefghijklmnopqrstuvwxyz1234567890'),

        'domain_links_limit' => env('DOMAIN_LINKS_LIMIT', 100),

        'user_links_limit' => env('USER_LINKS_LIMIT', 5000),
    ];

?>
<?php

namespace App\Observers;

use App\Helpers\Text;
use App\Models\BannedDomain;

class BannedDomainObserver
{
    public function saving(BannedDomain $bannedDomain)
    {
        $bannedDomain->host = trim(ltrim(Text::deleteHttp($bannedDomain->host), 'www.'), '/');
    }
}

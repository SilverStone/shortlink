<?php

namespace App\Observers;

use App\ConstantSets\LinkStatus;
use App\Helpers\LinkHelper;
use App\Models\Domain;
use App\Models\Link;

class LinkObserver
{
    public function saving(Link $link)
    {
        if (!$link->key) {
            $link->key = LinkHelper::generateKey();
        }

        if (!$link->status) {
            $link->status = LinkStatus::ENABLED;
        }

        if (!$link->domain_id) {
            $host = LinkHelper::hostByURL($link->target);
            if ($host) {
                if (!$domain = Domain::where('host', $host)->first()) {
                    $domain = new Domain(['host' => $host]);
                    $domain->save();
                }

                $link->domain_id = $domain->id;
            }
        }

        $link->key_length = mb_strlen($link->key);

        if ($link->isDirty('status') && $link->status === LinkStatus::DISABLED) {
            $link->disabled_at = date('Y-m-d H:i:s');
        }
    }
}

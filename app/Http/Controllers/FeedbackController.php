<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackFormRequest;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class FeedbackController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $html = view('feedback.create')->render();

        return response()->json([
            'html' => $html
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeedbackFormRequest $request)
    {
        $feedback = new Feedback($request->all());

        if (Auth::check()) {
            $feedback->user_id = Auth::id();
        }

        $feedback->save();

        Session::put('success', __('Сообщение успешно отрпвлено. Ожидайте ответа на указанный e-mail'));

        return response()->json([]);
    }
}

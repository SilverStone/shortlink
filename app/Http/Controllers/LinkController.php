<?php

namespace App\Http\Controllers;

use App\ConstantSets\LinkStatus;
use App\Helpers\LinkHelper;
use App\Http\Requests\LinkFormRequest;
use App\Http\Requests\LinkShowRequest;
use App\Models\Link;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LinkController extends Controller
{
    const SORTING_CREATED = 'created';
    const SORTING_EXPIRES = 'expires';

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store']]);
    }

    public function index(Request $request)
    {
        // icomment - тут нужен поиск по подстроке в кее и таргете

        $user = Auth::user();
        $links = $user->links();

        if ($sorting = $request->input('sorting')) {
            switch ($sorting) {
                case self::SORTING_EXPIRES:
                    $links = $links->orderBy('expires_at');
                    break;
            }
        }

        $links = $links->orderBy('created_at', 'desc')->paginate(100);

        $metaData = ['title' => __('Мои ссылки')];

        return view('link.index', [
            'links' => $links,
            'linksCount' => $user->links()->count(),
            'sortings' => [
                self::SORTING_CREATED => 'По дате создания',
                self::SORTING_EXPIRES => 'По дате активности',
            ],
            'sorting' => $sorting,
            'isPremium' => Auth::check() && Auth::user()->isPremium(),
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
            ]
        ]);
    }

    public function store(LinkFormRequest $request)
    {
        $currentUser = Auth::user();

        if ($currentUser instanceof User) {

            if ($currentUser->links()->count() >= config('common.user_links_limit')) {
                return redirect()->back()->withErrors(__('Достигнут лимит ссылок для данного аккаунта. Для увеличения лимита свяжитесь с администратором.'));
            }

            $userId = $currentUser->id;
        } else {
            $userId = null;
        }

        $link = new Link();
        $link->fill($request->all());
        $link->user_id = $userId;

        if ($request->filled('key') && $currentUser->isPremium()) {
            $link->key = trim($request->input('key'));
        }

        $lifeTimeDays = LinkHelper::lifeTimeDays($currentUser);
        $link->expires_at = date('Y-m-d H:i:s', time() + ($lifeTimeDays * 24 * 60 * 60));

        if ($link->save()) {
            return redirect()->back()->with('new-link', route('redirect', ['key' => $link->key]));
        } else {
            return redirect()->back()->withErrors('Ошибка сохранения ссылки');
        }
    }

    public function show(LinkShowRequest $request, $key)
    {
        if (!$link = Link::where('key', $key)->where('status', LinkStatus::ENABLED)->first()) {
            abort(404);
        }

        $this->authorize('view', $link);

        $metaData = ['title' => __('Ссылка').': '.$link->key];

        $begin = $request->filled('begin') ? strtotime($request->input('begin')) : time() - 30 * 24 * 60 * 60;
        $end = $request->filled('end') ? strtotime($request->input('end')) : time();

        $isPremium = Auth::check() && Auth::user()->isPremium();

        return view('link.show', [
            'link' => $link,
            'begin' => date('Y-m-d', $begin),
            'end' => date('Y-m-d', $end),
            'isPremium' => $isPremium,
            'shows' => LinkHelper::showsByPeriod($link, $begin, $end),
            'deviceTypes' => $isPremium ? LinkHelper::deviceTypesByPeriod($link, $begin, $end) : [],
            'browsers' => $isPremium ? LinkHelper::browsersByPeriod($link, $begin, $end) : [],
            'countries' => $isPremium ? LinkHelper::countriesByPeriod($link, $begin, $end) : [],
            'cities' => $isPremium ? LinkHelper::citiesByPeriod($link, $begin, $end) : [],
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
                __('Ссылки') => route('links.index'),
            ]
        ]);
    }

    public function prolong(Link $link)
    {
        $this->authorize('prolong', $link);

        $currentUser = Auth::user();
        $lifeTimeDays = LinkHelper::lifeTimeDays($currentUser);
        $link->expires_at = date('Y-m-d H:i:s', strtotime($link->expires_at) + ($lifeTimeDays * 24 * 60 * 60));
        $link->save();

        return [
            'link_id' => $link->id,
            'expires_at' => date('Y-m-d H:i', strtotime($link->expires_at)),
        ];
    }

    public function disable($key)
    {
        if (!$link = Link::where('key', $key)->first()) {
            abort(404);
        }

        $this->authorize('disable', $link);
        $link->status = LinkStatus::DISABLED;
        $link->save();
        return redirect()->back()->with('success', 'Успешно удалено');
    }

    public function destroy($key)
    {
        if (!$link = Link::where('key', $key)->first()) {
            abort(404);
        }

        $this->authorize('delete', $link);
        $link->delete();
        return redirect()->back()->with('success', 'Успешно удалено');
    }
}

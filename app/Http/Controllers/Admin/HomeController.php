<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $metaData = ['title' => __('Администрирование')];

        return view('admin.home.index', [
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
            ]
        ]);
    }
}

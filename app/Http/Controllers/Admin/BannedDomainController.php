<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BannedDomainFormReqest;
use App\Models\BannedDomain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannedDomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bannedDomains = BannedDomain::orderBy('host')->paginate(100);

        $metaData = ['title' => 'Запрещенные домены'];

        return view('admin.banned_domain.index', [
            'bannedDomains' => $bannedDomains,
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
                __('Администрирование') => route('admin.index'),
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bannedDomain = new BannedDomain();
        return $this->edit($bannedDomain);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannedDomainFormReqest $request)
    {
        $bannedDomain = new BannedDomain();
        return $this->update($request, $bannedDomain);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BannedDomain  $bannedDomain
     * @return \Illuminate\Http\Response
     */
    public function edit(BannedDomain $bannedDomain)
    {
        $metaData = ['title' => 'Заблокированные домены: '.($bannedDomain->id ? $bannedDomain->host : 'новый')];

        return view('admin.banned_domain.edit', [
            'bannedDomain' => $bannedDomain,
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
                'Заблокированные домены' => route('admin.banned-domains.index'),
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BannedDomain  $bannedDomain
     * @return \Illuminate\Http\Response
     */
    public function update(BannedDomainFormReqest $request, BannedDomain $bannedDomain)
    {
        $bannedDomain->fill($request->all());
        $bannedDomain->save();
        return redirect()
            ->route('admin.banned-domains.edit', $bannedDomain->id)
            ->with('success', 'Успешно сохранено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BannedDomain  $bannedDomain
     * @return \Illuminate\Http\Response
     */
    public function destroy(BannedDomain $bannedDomain)
    {
        $bannedDomain->delete();
        return redirect()
            ->route('admin.banned-domains.index')
            ->with('success', 'Успешно удалено');
    }
}

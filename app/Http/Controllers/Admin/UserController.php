<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UserHelper;
use App\Http\Requests\UserPremiumAddFormRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    const SORTING_NAME = 'name';
    const SORTING_PREMIUM_UNTIL = 'premium_until';

    public function index(Request $request)
    {
        $users = User::select(['*']);

        if ($sorting = $request->input('sorting')) {
            switch ($sorting) {
                case self::SORTING_PREMIUM_UNTIL:
                    $users = $users->orderBy('premium_until', 'desc');
                    break;
            }
        }

        $users = $users->orderBy('name')->paginate(100);

        $metaData = ['title' => 'Пользователи'];

        return view('admin.user.index', [
            'users' => $users,
            'sortings' => [
                self::SORTING_NAME => 'Имя',
                self::SORTING_PREMIUM_UNTIL => 'Премиум до',
            ],
            'sorting' => $sorting,
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
                __('Администрирование') => route('admin.index'),
            ]
        ]);
    }

    public function show(User $user)
    {
        $metaData = ['title' => 'Пользователь: '.$user->name];

        return view('admin.user.show', [
            'user' => $user,
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
                __('Администрирование') => route('admin.index'),
                'Пользователи' => route('admin.users.index'),
            ]
        ]);
    }

    public function premiumAdd(UserPremiumAddFormRequest $request, User $user)
    {
        $response = redirect()->back();

        if (UserHelper::premiumAdd($user, (int)$request->input('days'))) {
            $response = $response->with('success', 'Премиум период увеличен');
        } else {
            $response = $response->withErrors('Ошибка увеличения премиум периода');
        }

        return $response;
    }
}

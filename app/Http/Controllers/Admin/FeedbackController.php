<?php

namespace App\Http\Controllers\Admin;

use App\ConstantSets\UserParameter;
use App\Models\Feedback;
use App\Models\UserValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lastView = UserValue::get(Auth::user(), UserParameter::FEEDBACK_LAST_VIEW);
        UserValue::set(Auth::user(), UserParameter::FEEDBACK_LAST_VIEW, date('Y-m-d H:i:s'));

        $feedback = Feedback::orderBy('created_at', 'desc')->paginate(50);

        $metaData = ['title' => 'Обратная связь'];

        return view('admin.feedback.index', [
            'feedback' => $feedback,
            'lastView' => $lastView,
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
                __('Администрирование') => route('admin.index'),
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $feedback)
    {
        $metaData = ['title' => 'Сообщение от: '.$feedback->email];

        return view('admin.feedback.show', [
            'feedback' => $feedback,
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
                __('Администрирование') => route('admin.index'),
                __('Обратная связь') => route('admin.feedback.index'),
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedback)
    {
        $feedback->delete();
        return redirect()
            ->route('admin.feedback.index')
            ->with('success', 'Успешно удалено');
    }
}

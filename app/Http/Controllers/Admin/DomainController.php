<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DomainFormRequest;
use App\Models\Domain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DomainController extends Controller
{
    const SORTING_HOST = 'host';
    const SORTING_LINKS_COUNT = 'links_count';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $domains = Domain::select([
                'domains.id',
                'domains.host',
                'domains.personal_limit',
                DB::raw('COUNT(links.id) AS links_count'),
            ])
            ->leftJoin('links', function($join) {
                $join->on('domains.id', '=', 'links.domain_id');
            })
            ->groupBy(['domains.id', 'domains.host', 'domains.personal_limit']);

        if ($sorting = $request->input('sorting')) {
            switch ($sorting) {
                case self::SORTING_LINKS_COUNT:
                    $domains = $domains->orderBy('links_count', 'desc');
                    break;
            }
        }

        $domains = $domains->orderBy('host')->paginate(100);

        $metaData = ['title' => 'Домены'];

        return view('admin.domain.index', [
            'domains' => $domains,
            'sortings' => [
                self::SORTING_HOST => 'Хост',
                self::SORTING_LINKS_COUNT => 'Количество ссылок',
            ],
            'sorting' => $sorting,
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
                __('Администрирование') => route('admin.index'),
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function edit(Domain $domain)
    {
        $metaData = ['title' => 'Домен: '.$domain->host];

        return view('admin.domain.edit', [
            'domain' => $domain,
            'META_DATA' => $metaData,
            'BREADCRUMBS' => [
                env('APP_NAME') => route('home'),
                'Домены' => route('admin.domains.index'),
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function update(DomainFormRequest $request, Domain $domain)
    {
        $domain->fill($request->all());
        $domain->save();
        return redirect()
            ->route('admin.domains.edit', $domain->id)
            ->with('success', 'Успешно сохранено');
    }
}

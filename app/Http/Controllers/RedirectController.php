<?php

namespace App\Http\Controllers;

use App\ConstantSets\LinkStatus;
use App\Helpers\LinkHelper;
use App\Models\Link;
use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function redirect($key)
    {
        if (!$link = Link::where('key', $key)->where('status', LinkStatus::ENABLED)->first()) {
            abort(404);
        }

        // icomment - изучить очереди и попробовать добавить (хот есть ли смысл если сбор данных будет все равно до очереди)
        LinkHelper::writeShowStatistic($link);

        return redirect($link->target, 301)->header('Cache-Control', 'no-store, no-cache, must-revalidate');
    }
}

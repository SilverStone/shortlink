<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    // icomment

    // бан листы

    // страница "сокрашенные / короткие ссылки со статистикой" - подробное инфо о ссылках с скринами графиков и прочего

    // для премиума в отчеты по ссылкам можно добавить не только период, а выбор страны, города, девайс тайпа, браузера - сто даст смотреть более детальные отчеты

    // добавить покупку-пробник - 3 дня премиума - покупка доступна только один раз

    // индикатор/статус бар аккаунта (туда как раз убрать индикацию имени)

    // icomment - слерд релиз
    // нормальный диалог о удалении
    // подключение платежых апи

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metaData = [
            'title' => 'Сократить ссылку',
            'description' => 'LetsGo - сервис, который поможет сократить ссылку, собрать статистику переходов по сокращенной ссылке, а так же задать свой собственный короткий путь. Сервис позволяет создавать ссылки с условно вечной активностью.',
            'keywords' => 'сократить ссылку, ссылка сокращение, сокращенная ссылка',
        ];

        return view('home.index', [
            'META_DATA' => $metaData,
        ]);
    }

    public function about()
    {
        $metaData = [
            'title' => 'Короткая ссылка',
            'description' => 'Короткая ссылка - важнейший инструмент в современном мире и сервис LetsGo поможет сделать такую ссылку. Информация о различных вариантах использования и доступном функционале сервиса коротких ссылок на этой странице.',
            'keywords' => 'короткая ссылка',
        ];

        return view('home.about', [
            'META_DATA' => $metaData,
        ]);
    }

    public function rules()
    {
        $metaData = ['title' => __('Правила и условия пользования сервисом')];

        return view('home.rules', [
            'META_DATA' => $metaData,
        ]);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LinkFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $target = [
            'required',
            'string',
            'domain_valid',
            'max:1200',
        ];

        if (!Auth::check()) {
            $target[] = 'domain_limit';
        }

        return [
            'target' => implode('|', $target),
            'key' => 'nullable|custom_key|check_route'
        ];
    }

    public function attributes()
    {
        return [
            'target' => 'Ссылка',
            'key' => 'Желаемая ссылка',
        ];
    }

    public function messages()
    {
        return [
            'target.domain_valid' => 'Ссылка некорректна или домен находится в списке запрещенных',
            'target.domain_limit' => 'Для указанного домена превышен лимит ссылок, по достижении которого гость может создавать коротки ссылки.
             Создание коротких ссылок для данного домена доступно для зарегистрированных пользователей.',
            'key.custom_key' => 'Желаемая ссылка может содержать только латинские буквы в нижнем регистре, цифры, знак 
            подчеркивания и дефис. Длина ссылки должна быть от 4 до 30 символов.',
            'key.check_route' => 'Желаемая ссылка занята системным маршрутом',
        ];
    }
}

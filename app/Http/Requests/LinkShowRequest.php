<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LinkShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'begin' => 'nullable|date',
            'end' => 'date_after_if_not_null:'.$this->input('begin').'|nullable|date',
        ];
    }

    public function messages()
    {
        return [
            'end.date_after_if_not_null' => __('Дата конца должна быть больше даты начала'),
        ];
    }
}

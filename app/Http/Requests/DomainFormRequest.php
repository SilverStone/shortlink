<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DomainFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'personal_limit' => 'nullable|integer|min:0|max:32766',
        ];
    }

    public function attributes()
    {
        return [
            'personal_limit' => 'Персональный лимит',
        ];
    }
}

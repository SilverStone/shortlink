<?php

namespace App\Console\Commands;

use App\ConstantSets\LinkStatus;
use App\Models\Link;
use Illuminate\Console\Command;

class LinksDisable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'links:disable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable expired links';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $links = Link::where([
            ['expires_at', '<=', date('Y-m-d 00:00:00')],
            ['status', '=', LinkStatus::ENABLED],
        ])->get();

        $i = 0;
        foreach ($links as $link) {
            $i++;
            $link->status = LinkStatus::DISABLED;
            $link->save();
        }

        $this->info('Disabled '.$i.' links');
    }
}

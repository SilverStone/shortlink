<?php

namespace App\Console\Commands;

use App\ConstantSets\LinkStatus;
use App\Models\Link;
use Illuminate\Console\Command;

class LinksDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'links:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete disabled links';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $border = time() - (24 * 60 * 60 * (int)config('common.link_clear_disabled_after'));
        $border = date('Y-m-d 00:00:00', $border);

        $links = Link::where([
            ['disabled_at', '<=', $border],
            ['status', '=', LinkStatus::DISABLED],
        ])->get();

        $i = 0;
        foreach ($links as $link) {
            $i++;
            $link->delete();
        }


        $this->info('Deleted '.$i.' links');
    }
}

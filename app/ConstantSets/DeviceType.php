<?php

namespace App\ConstantSets;

use Fsmdev\ConstantsCollection\ConstantsCollection;

class DeviceType extends ConstantsCollection
{
    const DESKTOP = 10;
    const TABLET = 20;
    const MOBILE = 30;
    const ROBOT = 40;

    protected static function init__name()
    {
        return [
            self::DESKTOP => 'ПК',
            self::TABLET => 'Планшет',
            self::MOBILE => 'Мобильный',
            self::ROBOT => 'Робот',
        ];
    }
}
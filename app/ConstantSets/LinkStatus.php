<?php

namespace App\ConstantSets;

use Fsmdev\ConstantsCollection\ConstantsCollection;

class LinkStatus extends ConstantsCollection
{
    const ENABLED = 10;
    const DISABLED = 20;

    protected static function init__name()
    {
        return [
            self::ENABLED => __('Активна'),
            self::DISABLED => __('Не активна'),
        ];
    }
}
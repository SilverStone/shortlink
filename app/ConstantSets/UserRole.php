<?php

namespace App\ConstantSets;


use Fsmdev\ConstantsCollection\ConstantsCollection;

class UserRole extends ConstantsCollection
{
    const USER = 10;
    const ADMIN = 20;

    protected static function init__name()
    {
        return [
            self::USER => 'Пользователь',
            self::ADMIN => 'Администратор',
        ];
    }
}
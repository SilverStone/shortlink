<?php

namespace App\ConstantSets;

use Fsmdev\ConstantsCollection\ConstantsCollection;

class UserParameter extends ConstantsCollection
{
    const FEEDBACK_LAST_VIEW = 50;

    protected static function init__name()
    {
        return [
            self::FEEDBACK_LAST_VIEW => __('Последний просмотр обратной связи'),
        ];
    }
}
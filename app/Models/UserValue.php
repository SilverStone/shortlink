<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserValue extends Model
{
    protected $fillable = [
        'user_id',
        'parameter',
        'value',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function get(User $user, $parameter)
    {
        $userValue = self::getModel($user, $parameter);
        return $userValue ? $userValue->value : null;
    }

    public static function set(User $user, $parameter, $value)
    {
        if (!$userValue = self::getModel($user, $parameter)) {
            $userValue = new UserValue([
                'user_id' => $user->id,
                'parameter' => $parameter,
            ]);
        }

        $userValue->value = $value;
        return $userValue->save();
    }

    protected static function getModel(User $user, $parameter)
    {
        return UserValue::where([
            'user_id' => $user->id,
            'parameter' => $parameter,
        ]) ->first();
    }
}

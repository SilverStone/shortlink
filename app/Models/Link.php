<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = [
        'status',
        'user_id',
        'target',
        'expires_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function domain()
    {
        return $this->belongsTo('App\Models\Domain');
    }

    public function inProlongPeriod()
    {
        return date('Ymd', time() + (24 * 60 * 60 * (int)config('common.link_prolong_period')))
            >= date('Ymd', strtotime($this->expires_at));
    }
}

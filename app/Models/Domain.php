<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
        'host',
        'personal_limit',
    ];

    public function links()
    {
        return $this->hasMany('App\Models\Link');
    }
}

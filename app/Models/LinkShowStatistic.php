<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinkShowStatistic extends Model
{
    const UPDATED_AT = null;

    protected $primaryKey = null;

    public $incrementing = false;

    protected $fillable = [
        'link_id',
        'ip',
        'country_iso_code',
        'country',
        'city',
        'platform',
        'device_type',
        'browser',
        'browser_version',
    ];

    public function setUpdatedAt($value)
    {
        # Do nothing
    }
}

<?php

namespace App\Providers;


use App\Helpers\LinkHelper;
use App\Models\BannedDomain;
use App\Models\Domain;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidatorsServiseProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('password', function($attribute, $value, $parameters) {
            return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[\S]{8,}$/', $value);
        });

        Validator::extend('custom_key', function($attribute, $value, $parameters) {
            return preg_match('/^[a-z0-9_\-]{4,30}$/', trim($value));
        });

        Validator::extend('check_route', function($attribute, $value, $parameters) {
            $routes = [];
            foreach (Route::getRoutes()->getIterator() as $route){
                $routes[] = $route->uri;
            }
            return !in_array(trim($value), $routes, true);
        });

        Validator::extend('domain_limit', function($attribute, $value, $parameters) {
            $result = true;
            $host = LinkHelper::hostByURL($value);

            if ($host) {
                if ($domain = Domain::where('host', $host)->first()) {
                    $limit = $domain->personal_limit ? $domain->personal_limit : config('common.domain_links_limit');
                    if ($limit <= $domain->links()->count()) {
                        $result = false;
                    }
                }
            }

            return $result;
        });

        Validator::extend('domain_valid', function($attribute, $value, $parameters) {
            $result = false;
            $host = LinkHelper::hostByURL($value);
            if ($host) {
                if (!$domain = BannedDomain::where('host', $host)->first()) {
                    $result = true;
                }
            }
            return $result;
        });

        Validator::extend('date_after_if_not_null', function($attribute, $value, $parameters) {
            return strtotime($value) > strtotime($parameters[0]);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

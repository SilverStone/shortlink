<?php

namespace App;

use App\ConstantSets\LinkStatus;
use App\ConstantSets\UserRole;
use App\Models\Link;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $isPremium = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function links()
    {
        return $this->hasMany('App\Models\Link')->where('status', LinkStatus::ENABLED);
    }

    public function isAdmin()
    {
        return $this->role === UserRole::ADMIN;
    }

    public function isPremium()
    {
        if (is_null($this->isPremium)) {
            $this->isPremium = time() < strtotime($this->premium_until);
        }

        return $this->isPremium;
    }

    public function isBanned()
    {
        // icomment - нормальный механизм банов
        return false;
    }
}

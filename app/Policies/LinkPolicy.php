<?php

namespace App\Policies;

use App\User;
use App\Models\Link;
use Illuminate\Auth\Access\HandlesAuthorization;

class LinkPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function view(User $user, Link $link)
    {
        return $user->id === $link->user_id;
    }

    public function prolong(User $user, Link $link)
    {
        return $user->id === $link->user_id && $link->inProlongPeriod() && $user->isPremium();
    }

    public function disable(User $user, Link $link)
    {
        return $user->id === $link->user_id;
    }

    public function delete(User $user, Link $link)
    {
        # Only admin can delete links
        return false;
    }
}


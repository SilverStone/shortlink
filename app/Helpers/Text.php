<?php

namespace App\Helpers;


class Text
{
    static function stringToFloat($string, $precision = 2)
    {
        return round((float)strtr($string, array("," => ".", " " => "")), $precision);
    }

    static function floatToString($float, $precision = 2, $emptyDecimals = false, $separator = " ", $dot = ".")
    {
        $float = self::stringToFloat($float, $precision);
        $string = number_format($float, $precision, $dot, $separator);
        if (!$emptyDecimals) {
            while (!(int)mb_substr($string, -1)) {
                $isDot = mb_substr($string, -1) === $dot;
                $string = mb_substr($string, 0, mb_strlen($string) - 1);
                if ($isDot) break;
            }
        }
        return $string;
    }

    static function boolToYesNo($value)
    {
        return (bool)$value ? "Да" : "Нет";
    }

    static function dayOutput($date = false)
    {
        if (!$date) $date = time();
        if($date >= strtotime(date("Y-m-d 00:00:00")) && $date <= strtotime(date("Y-m-d 23:59:59"))) return "сегодня";
        $time = time() - (24 * 60 * 60);
        if($date >= strtotime(date("Y-m-d 00:00:00", $time)) && $date <= strtotime(date("Y-m-d 23:59:59", $time))) return "вчера";
        return Text::ruDate("", $date);
    }

    static function ruDate($format = "", $date = false)
    {
        if (!$date) $date = time();
        if (!$format) $format = "%e %bg %Y";
        $months = explode("|", '|января|февраля|марта|апреля|мая|июня|июля|августа|сентября|октября|ноября|декабря');
        $format = preg_replace("~\%bg~", $months[date("n", $date)], $format);
        $result = strftime($format, $date);
        $result = strtr($result, array("&nbsp" => " "));
        return $result;
    }

    static function deleteHttp($str)
    {
        return strtr($str, array("http://" => "", "https://" => ""));
    }

    static function stringLength($str)
    {
        return mb_strlen(str_replace(array("&#13;&#10;", "&#10;&#13;", "&#10;", "&#13;", " "), "", $str));
    }

    static function translit($st)
    {
        $st = strtr($st, array("онлайн"=>"online"));
        $st = strtr($st, array("&#39;"=>""));
        $st = preg_replace("/[^а-яА-Яa-zA-Z0-9\- ]/u", "", $st);
        $st = strtr($st, array(' '=>'-', 'Q'=>'q', 'A'=>'a', 'Z'=>'z', 'W'=>'w', 'S'=>'s', 'X'=>'x', 'E'=>'e', 'D'=>'d', 'C'=>'c', 'R'=>'r', 'F'=>'f', 'V'=>'v', 'T'=>'t', 'G'=>'g', 'B'=>'b', 'Y'=>'y', 'H'=>'h', 'N'=>'n', 'U'=>'u', 'J'=>'j', 'M'=>'m', 'I'=>'i', 'K'=>'k', 'O'=>'o', 'L'=>'l', 'P'=>'p'));
        $st = strtr($st, array('а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'e', 'ё'=>'e', 'з'=>'z', 'и'=>'i', 'й'=>'j', 'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o', 'п'=>'p', 'р'=>'r', 'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'х'=>'h', 'ц'=>'c', 'ы'=>'y', 'э'=>'e'));
        $st = strtr($st, array('А'=>'a', 'Б'=>'b', 'В'=>'v', 'Г'=>'g', 'Д'=>'d', 'Е'=>'e', 'Ё'=>'e', 'З'=>'z', 'И'=>'i', 'Й'=>'j', 'К'=>'k', 'Л'=>'l', 'М'=>'m', 'Н'=>'n', 'О'=>'o', 'П'=>'p', 'Р'=>'r', 'С'=>'s', 'Т'=>'t', 'У'=>'u', 'Ф'=>'f', 'Х'=>'h', 'Ц'=>'c', 'Ы'=>'y', 'Э'=>'e'));
        $st = strtr($st, array("Щ"=>"shch", "щ"=>"shch", "Ш"=>"sh", "ш"=>"sh", "Ъ"=>"", "ъ"=>"", "Ь"=>"", "ь"=>"", "Ю"=>"yu", "ю"=>"yu", "Я"=>"ya", "я"=>"ya", "Ж"=>"zh", "ж"=>"zh", "Ч"=>"ch", "ч"=>"ch"));
        $st = strtr($st, array("--"=>"-"));
        $st = strtr($st, array("--"=>"-"));
        $st = strtr($st, array("--"=>"-"));
        $st = strtr($st, array("--"=>"-"));
        $st = strtr($st, array("--"=>"-"));
        return $st;
    }

    static function deleteBB($str)
    {
        return preg_replace("/\[.+\]/Us", "", $str);
    }

    static function wordform($count, $wordforms)
    {
        # 0 - no form
        # 1 - base form
        # 2 - 2nd form
        # 3 - 3d form
        $count = (int)$count;
        $count100 = abs($count) % 100;
        $count10 = abs($count) % 10;
        $index = 0;
        if ($count100 > 10 && $count100 < 20) $index = 3;
        elseif ($count10 > 1 && $count10 < 5) $index = 2;
        elseif ($count10 == 1) $index = 1;
        else $index = 3;
        return $wordforms[$index];
    }

    static function replaceOnce($search, $replace, $text)
    {
        $pos = mb_strpos($text, $search, 0);
        return $pos !== false ? self::mb_substrReplace($text, $replace, $pos, mb_strlen($search)) : $text;
    }

    static function mb_substrReplace($string, $replacement, $start, $length = null, $encoding = null)
    {
        if($encoding == null) $encoding = Properties::ENCODING;
        if($length == null) {
            return mb_substr($string, 0, $start, $encoding).$replacement;
        } else {
            if($length < 0) $length = mb_strlen($string, $encoding) - $start + $length;
            return
                mb_substr($string, 0, $start, $encoding).$replacement.mb_substr($string, $start + $length, mb_strlen($string, $encoding), $encoding);
        }
    }

    public static function awayHref($href) {

        $href = strtr(self::deleteHttp($href), [
            '.' => '-pnt-',
            '=' => '-eq-',
            '?' => '-qe-',
            '&' => '-and-',
            '/' => '-sl-',
        ]);

        return route('away', $href);

    }
}
<?php

namespace App\Helpers;

use App\ConstantSets\UserParameter;
use App\Models\Feedback;
use App\Models\UserValue;
use App\User;

class UserHelper
{
    public static function premiumAdd($user, $days)
    {
        // icomment - лог премиум периода

        if (!$user instanceof User) {
            if (!$user = User::where('id', $user)->first()) {
                return false;
            }
        }

        $start = $user->isPremium() ? strtotime($user->premium_until) : time();
        $user->premium_until = date('Y-m-d H:i:s', $start + ($days * 24 * 60 * 60));
        return $user->save();
    }

    public static function newFeedbackCount(User $user)
    {
        $lastView = UserValue::get($user, UserParameter::FEEDBACK_LAST_VIEW);
        $lastView = $lastView ? $lastView : '0000-00-00';
        return Feedback::where('created_at', '>=', $lastView)->count();
    }
}
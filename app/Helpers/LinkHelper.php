<?php

namespace App\Helpers;


use App\ConstantSets\DeviceType;
use App\Models\Link;
use App\Models\LinkShowStatistic;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;

class LinkHelper
{
    public static function generateKey($params = [])
    {
        $length = isset($params['length']) ? $params['length'] : self::keyLength();

        $chairs = str_split(config('common.link_symbols_string'));
        $maxIndex = count($chairs) - 1;

        $iteration = 0;
        do {
            $iteration++;

            if ($iteration > config('common.link_generate_iteration_limit')) {
                $iteration = 0;
                $length++;
            }

            $key = '';
            for ($i = 0; $i < $length; $i++) {
                $key .= $chairs[rand(0, $maxIndex)];
            }

        } while(Link::where('key', $key)->first());

        return $key;
    }

    public static function keyLength($user = null)
    {
        if (!$user) {
            $user = Auth::user();
        }

        return ($user instanceof User)
            ?
            ($user->isPremium() ? config('common.link_length_premium') : config('common.link_length_registered'))
            :
            config('common.link_length_guest');
    }

    public static function lifeTimeDays($user = null)
    {
        if (!$user) {
            $user = Auth::user();
        }

        return ($user instanceof User)
            ?
            ($user->isPremium() ? config('common.link_lifetime_premium') : config('common.link_lifetime_registered'))
            :
            config('common.link_lifetime_guest');
    }

    public static function writeShowStatistic(Link $link)
    {
        if (!$link->user_id) return;

        $agent = new Agent();

        $platform = $agent->platform();
        $browser = $agent->browser();

        $deviceType = $agent->isRobot()
            ?
            DeviceType::ROBOT
            :
            (
                $agent->isTablet()
                ?
                DeviceType::TABLET
                :
                ($agent->isMobile() ? DeviceType::MOBILE : DeviceType::DESKTOP)
            );

        $location = geoip()->getLocation();

        $data = [
            'link_id' => $link->id,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'country_iso_code' => ($location['default'] ? null : $location['iso_code']),
            'country' => ($location['default'] ? null : $location['country']),
            'city' => ($location['default'] ? null : $location['city']),
            'platform' => $platform,
            'device_type' => $deviceType,
            'browser' => $browser,
            'browser_version' => $agent->version($browser),
        ];

        LinkShowStatistic::create($data);
    }

    public static function hostByURL($url)
    {
        $host = null;
        $targetParts = parse_url($url);
        if (isset($targetParts['host'])) {
            $host = ltrim($targetParts['host'], 'www.');
        }
        return $host;
    }

    public static function showsByPeriod(Link $link, $begin, $end)
    {
        $sql = 'SELECT
                DATE_FORMAT(`link_show_statistics`.`created_at`, "%Y-%m-%d") AS `day`,
                SUM(1) AS `count`
                FROM `link_show_statistics`
                WHERE `link_show_statistics`.`link_id` = :link_id
                  AND `link_show_statistics`.`created_at` BETWEEN :date_begin AND :date_end                
                GROUP BY `day`
                ORDER BY `day`';

        $dbResult = DB::select(DB::raw($sql), [
            'link_id' => $link->id,
            'date_begin' => date('Y-m-d 00:00:00', $begin),
            'date_end' => date('Y-m-d 23:59:59', $end),
        ]);

        $data = [];
        foreach ($dbResult as $record) {
            $data[$record->day] = $record->count;
        }
        unset($dbResult);

        $day = date('Y-m-d', $begin);
        $endDay = date('Y-m-d', $end);

        $days = ceil(($end - $begin) / (24 * 60 * 60));

        if ($days > 364) {
            $format = 'd.m.y';
        } elseif ($days > 27)  {
            $format = 'd.m';
        } else {
            $format = 'd';
        }

        $result = [];

        while($day <= $endDay) {
            $dayTimestamp = strtotime($day);
            $result[date($format, $dayTimestamp)] = isset($data[$day]) ? (int)$data[$day] : 0;
            $day = date('Y-m-d', $dayTimestamp + (24 * 60 * 60) + 100);
        }

        return $result;
    }

    public static function deviceTypesByPeriod(Link $link, $begin, $end)
    {
        $sql = 'SELECT
                `link_show_statistics`.`device_type` AS `device_type`,
                SUM(1) AS `count`
                FROM `link_show_statistics`
                WHERE `link_show_statistics`.`link_id` = :link_id
                  AND `link_show_statistics`.`created_at` BETWEEN :date_begin AND :date_end                
                GROUP BY `device_type`
                ORDER BY `count` DESC';

        $dbResult = DB::select(DB::raw($sql), [
            'link_id' => $link->id,
            'date_begin' => date('Y-m-d 00:00:00', $begin),
            'date_end' => date('Y-m-d 23:59:59', $end),
        ]);

        $data = [];
        foreach ($dbResult as $record) {
            $data[DeviceType::property($record->device_type)] = $record->count;
        }

        return $data;
    }

    public static function browsersByPeriod(Link $link, $begin, $end)
    {
        return self::simpleStatisticFieldByPeriod('browser', $link, $begin, $end);
    }

    public static function countriesByPeriod(Link $link, $begin, $end)
    {
        return self::simpleStatisticFieldByPeriod('country', $link, $begin, $end);
    }

    public static function citiesByPeriod(Link $link, $begin, $end)
    {
        return self::simpleStatisticFieldByPeriod('city', $link, $begin, $end);
    }

    protected static function simpleStatisticFieldByPeriod($field, Link $link, $begin, $end)
    {
        $sql = 'SELECT
                `link_show_statistics`.`'.$field.'` AS `key`,
                SUM(1) AS `count`
                FROM `link_show_statistics`
                WHERE `link_show_statistics`.`link_id` = :link_id
                  AND `link_show_statistics`.`created_at` BETWEEN :date_begin AND :date_end                
                GROUP BY `key`
                ORDER BY `count` DESC';

        $dbResult = DB::select(DB::raw($sql), [
            'link_id' => $link->id,
            'date_begin' => date('Y-m-d 00:00:00', $begin),
            'date_end' => date('Y-m-d 23:59:59', $end),
        ]);

        $data = [];
        foreach ($dbResult as $record) {
            $data[($record->key ? $record->key : __('Неопределено'))] = $record->count;
        }

        return self::calculateOtherInResult($data);
    }

    protected static function calculateOtherInResult($data)
    {
        if (count($data) <= 7) {
            return $data;
        }

        $result = [];
        $i = 0;
        $other = 0;

        foreach ($data as $key => $value) {
            $i++;

            if ($i < 7) {
                $result[$key] = $value;
            } else {
                $other += $value;
            }
        }

        $result[__('Другое')] = $other;

        return $result;
    }
}
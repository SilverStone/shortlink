<?php

namespace App\Helpers;

use Illuminate\Support\Facades\URL;

class Page
{
    public static function canonical($params = [])
    {
        asort($params);
        $get = [];

        foreach($params as $param) {
            if (isset($_GET[$param])) {
                $get[] = $param.'='.$_GET[$param];
            }
        }

        return URL::current().(count($get) ? '?'.implode('&', $get) : '');
    }
}
require('./bootstrap');
require('../../node_modules/jquery-mask-plugin/dist/jquery.mask.min');
require('../../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min');

import Chart from '../../node_modules/chart.js/dist/Chart.min';

$(document).ready(function () {

    var MODAL_FORM_LOCK = false;

    $('[sorting-field]').change(function() {
        $('#' + $(this).attr('form')).submit();
    });

    $('#show-custom-link-key').click(function() {
        $(this).closest('div').hide();
        $('#custom-link-key-wrap').show();
    });

    $('[copy-to-clipboard]').click(function() {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(this).attr('copy-to-clipboard')).select();
        document.execCommand("copy");
        $temp.remove();
    });

    $('[delete-form]').submit(function() {
        return confirm('Удалить выбранный объект?');
    });

    $('[link-prolong]').click(function() {
        $.ajax({
            url: $(this).attr('path'),
            method: 'get',
            success: function(response) {
                $('[link-prolong-value="' + response.link_id + '"]').html(response.expires_at)
            }
        });
    });

    $(document).on('submit', '[modal-form]', function (event) {
        event.preventDefault();

        if (MODAL_FORM_LOCK) {
            return false;
        }

        MODAL_FORM_LOCK = true;

        var form = $(this);
        var formData = new FormData($(this)[0]);

        $.ajax({
            url     : form.attr('action'),
            type    : form.attr('method'),
            data    : formData,
            processData: false,
            contentType: false,
            success : function (response, textStatus, jqXHR)
            {
                if (response.blank) {
                    var win = window.open(response.blank, '_blank');
                }

                if (response.redirect) {
                    window.location.href = response.redirect;
                    return;
                }

                window.location.reload();
            },
            error: function(response)
            {
                MODAL_FORM_LOCK = false;

                if(response.status === 422) {

                    var message = '';

                    $.each(response.responseJSON.errors, function (field, errors) {
                        $.each(errors, function (i, error) {
                            message += '<li>'+error+'</li>';
                        });
                    });

                    var modalErrors = $('#modal-errors');

                    modalErrors.html(message);
                    modalErrors.closest('div').show();
                    $('#modal-wrap').animate({ scrollTop: 0 }, 'slow');

                } else {
                    $('#modal-wrap').modal('hide');
                }
            }
        });

        return false;
    });

    $(document).on('click', '[modal-call]', function (event) {
        event.preventDefault();

        var data = {};

        $.each($(this).data(), function (key, value) {
            data[key] = value;
        });

        modalRequest($(this).attr('path'), data);
    });

    initMask();
    initDatepicker();
    chartsDraw();
});

function initMask() {
    $('[mask-date]').mask('0000-00-00');
}

function initDatepicker() {

    $.fn.datepicker.dates['ru'] = {
        days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
        daysShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
        daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
        today: "Сегодня"
    };

    $('.datepicker').each(function() {
        $(this).datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true,
            ignoreReadonly: true,
            allowInputToggle: true,
            clearBtn: false,
            disableTouchKeyboard: true,
            isRTL: false,
            language: 'ru',
            weekStart: 1
        });
    });
}

function chartsDraw() {

    var colorsBase = [
        '#42A5F5',
        '#388E3C',
        '#7B1FA2',
        '#E64A19',
        '#FFA000',
        '#455A64',
        '#5D4037',
    ];

    $('[chart-js-line]').each(function() {

        var axisX = $(this).attr('axis-x').split(';');
        var axisY = $(this).attr('axis-y').split(';');

        var chart = new Chart($(this), {

            type: 'line',

            data: {
                labels: axisX,
                datasets: [{
                    backgroundColor: '#E3F2FD',
                    borderColor: '#42A5F5',
                    lineTension: 0,
                    data: axisY,
                }]
            },

            options: {
                legend: {
                    display: false,
                }
            }
        });
    });

    $('[chart-js-doughnut]').each(function() {

        var axisX = $(this).attr('axis-x').split(';');
        var axisY = $(this).attr('axis-y').split(';');

        var chart = new Chart($(this), {

            type: 'doughnut',

            data: {
                labels: axisX,
                datasets: [{
                    backgroundColor: colorsBase,
                    lineTension: 0,
                    data: axisY,
                }]
            },

            options: {
                legend: {
                    display: true,
                }
            }
        });
    });
}

function modalRequest(url, data)
{
    $.get(url, data, showModalResponse, 'json');
}

function showModalResponse(response, textStatus, jqXHR)
{
    $('#modal-wrap').html(response.html);
    $('#modal-wrap').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });
}

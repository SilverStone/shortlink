@if(\Session::has('success'))
    <div class="container mt-3">
        <div class="alert alert-success">
            {{\Session::get('success')}}
        </div>
    </div>

    @php
        \Session::remove('success')
    @endphp

@endif
@isset($BREADCRUMBS)
    <div class="container breadcrumbs mb-2">
        @foreach($BREADCRUMBS as $text => $href)
            <a href="{{ $href }}">{{ $text }}</a>
        @endforeach
    </div>
@endif
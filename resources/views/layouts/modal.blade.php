<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">@yield('title')</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="alert alert-danger" style="display: none;">
                <ul id="modal-errors">
                </ul>
            </div>
            @yield('body')
        </div>
        <div class="modal-footer">
            @yield('footer')
        </div>
    </div>
</div>
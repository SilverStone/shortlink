<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="canonical" href="{{ Page::canonical() }}"/>
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

@include('common.meta_title')
@include('common.meta_description')
@include('common.meta_keywords')

<!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @if(env('APP_ENV') === 'production')
        @include('common.head_includes')
    @endif

</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-custom @yield('header-nav-class', '')">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="fa fa-bars"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav">

                    <li class="nav-item">
                        <span class="nav-link" modal-call path="{{ route('feedback.create') }}">
                            {{ __('Обратная связь') }}
                        </span>
                    </li>

                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">
                                {{ __('Вход') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            @if (Route::has('register'))
                                <a class="nav-link" href="{{ route('register') }}">
                                    {{ __('Регистрация') }}
                                </a>
                            @endif
                        </li>
                    @else

                        @if(Auth::user()->isAdmin())
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('admin.index') }}">
                                    {{ __('A') }}
                                </a>
                            </li>
                        @endif

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('links.index') }}">
                                {{ __('Мои ссылки') }}
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Выход') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>

                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main>
        @include('common.breadcrumbs')
        @include('common.errors')
        @include('common.success')
        @yield('content')
    </main>

    <div class="modal fade" id="modal-wrap" role="dialog" aria-hidden="true">
    </div>
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

</div>
</body>
</html>

@extends('layouts.app')

@section('content')
<div class="container">

    <h1 class="centred">{{ __('Вход') }}</h1>

    <div class="row justify-content-center">
        <div class="col-md-6 col-sm-12">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group">
                    <label for="email">E-Mail:</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" autocomplete="off" value="{{ old('email') }}" autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password">{{ __('Пароль') }}</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                            {{ __('Запомнить меня') }}
                        </label>
                    </div>
                </div>

                <div class="form-group text-center mt-4 mb-0">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('Войти') }}
                    </button>
                </div>

                <div class="form-group text-center mt-4 mb-0">
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Забыли пароль?') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <h1 class="centred">{{ __('Восстановление пароля') }}</h1>

            <div class="col-md-6 col-sm-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <div class="form-group">
                        <label for="email">{{ __('E-Mail') }}:</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" autocomplete="off" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email">{{ __('Пароль') }}:</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div c1lass="form-group">
                        <label for="email">{{ __('Пароль (повторно)') }}:</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>

                    <div class="form-group text-center mt-4 mb-0">
                        <button type="submit" class="btn btn-lg btn-primary">
                            {{ __('Сменить пароль') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
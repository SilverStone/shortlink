@extends('layouts.app')

@section('content')
<div class="container">

    <h1 class="centred">{{ __('Регистрация') }}</h1>

    <div class="row justify-content-center">
        <div class="col-md-6 col-sm-12">

            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="form-group">
                    <label for="email">E-Mail:</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" autocomplete="off" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password">Пароль:</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password-confirm">Пароль повторно:</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                </div>

                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="site_rules" name="site_rules">
                    <label class="form-check-label" for="site_rules">
                        Я согласен с <a href="{{ route('rules') }}" target="_blank">правилами</a> использования сервиса
                    </label>
                </div>

                <div class="form-group text-center mt-4 mb-0">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Зарегистрироваться
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

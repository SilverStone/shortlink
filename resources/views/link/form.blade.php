@if(\Session::has('new-link'))

    <div class="link-view text-center">
        <a href="{{\Session::get('new-link')}}" target="_blank">{{\Session::get('new-link')}}</a>
        <i class="btn btn-link fa fa-copy" copy-to-clipboard="{{\Session::get('new-link')}}" title="{{ __('Скопировать в буфер') }}"></i>
    </div>

    <p class="text-center">

        Срок активности ссылки  <b>{{ LinkHelper::lifeTimeDays() }} дней</b>.

        @if(!Auth::check())
            <a href="{{ route('register') }}">Зарегистрированные</a> пользователи могут создавать ссылки с большим
            сроком активности, более короткой длины, а также видеать статистику переходов по ссылке.
        @elseif(Auth::user()->isPremium())
            Обладатели премиум-аккаунта могут продлевать срок активности ссылки за
            {{ config('common.link_prolong_period') }} дней до его истечения.
        @else
            Обладатели премиум-аккаунта могут продлевать срок активности ссылки за
            {{ config('common.link_prolong_period') }} дней до его истечения,
            а также видеть расширенную статистику ссылок.
        @endif
    </p>

@else

    {{ Form::open(['url' => route('links.store'), 'method' => 'POST']) }}

        @if(Auth::check() && Auth::user()->isPremium())
            @include('link.form_premium')
        @else
            @include('link.form_short')
        @endif

    {{ Form::close() }}

@endif
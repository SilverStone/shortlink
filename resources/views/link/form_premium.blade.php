<div class="row">
    <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 offset-sm-0">
        <div class="form-group">
            {{ Form::text('target', null, ['class' => 'form-control '.($errors->has('target') ? ' is-invalid' : '' ), 'autocomplete' => 'off']) }}
        </div>

        <div class="form-group text-center" style="display: {{ old('key') ? 'none' : 'block' }};">
            <button type="button" class="btn btn-link" id="show-custom-link-key">Указать желаемый адрес ссылки</button>
        </div>

        <div class="form-group text-center" style="display: {{ old('key') ? 'block' : 'none' }};" id="custom-link-key-wrap">
            <label>Желаемый короткий адрес:</label>
            {{ Form::text('key', null, ['class' => 'form-control '.($errors->has('key') ? ' is-invalid' : '' ), 'autocomplete' => 'off']) }}
        </div>

        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary btn-lg">СОКРАТИТЬ</button>
        </div>
    </div>
</div>
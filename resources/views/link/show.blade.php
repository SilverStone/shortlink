@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        <div class="row mb-3">
            <div class="col-md-4 col-sm-12">
                <a href="{{ route('redirect', $link->key) }}" target="_blank">
                    {{ route('redirect', $link->key) }}
                </a>
                <i class="btn btn-link fa fa-copy" copy-to-clipboard="{{ route('redirect', $link->key) }}" title="{{ __('Скопировать в буфер') }}"></i>
            </div>
            <div class="col-md-4 col-sm-12">
                {{ __('Создана') }}: {{ date('Y-m-d H:i', strtotime($link->created_at)) }}
            </div>
            <div class="col-md-4 col-sm-12">
                {{ __('Активна до') }}: {{ date('Y-m-d H:i', strtotime($link->expires_at)) }}
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-body pb-1">
                        <div class="card-title">
                            <h3>{{ __('Настройки') }}</h3>
                        </div>
                        <form method="get">
                            <div class="row">
                                <div class="col-md-4 col-sm-12 form-group">
                                    <p class="mb-1">{{ __('Начало периода') }}:</p>
                                    {{ Form::text('begin', (isset($begin) ? $begin : old('begin')), [
                                        'class' => 'form-control datepicker',
                                        'placeholder' => 'ГГГГ-ММ-ДД',
                                        'mask-date' => 1,
                                        'autocomplete' => 'off',
                                    ]) }}
                                </div>
                                <div class="col-md-4 col-sm-12 form-group">
                                    <p class="mb-1">{{ __('Конец периода') }}:</p>
                                    {{ Form::text('end', (isset($end) ? $end : old('end')), [
                                        'class' => 'form-control datepicker',
                                        'placeholder' => 'ГГГГ-ММ-ДД',
                                        'mask-date' => 1,
                                        'autocomplete' => 'off',
                                    ]) }}
                                </div>
                                <div class="col-md-4 col-sm-12 form-group">
                                    <p class="hide-on-mobile mb-1">&nbsp;</p>
                                    <button type="submit" class="btn btn-primary">{{ __('Показать') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h3>{{ __('Количество переходов') }}</h3>
                        </div>
                        @if($shows)
                            <canvas
                                    id="link-visits-chart"
                                    chart-js-line
                                    axis-x="{{ implode(';', array_keys($shows)) }}"
                                    axis-y="{{ implode(';', $shows) }}"
                            ></canvas>
                        @else
                            <p class="text-center text-black-50">
                                {{ __('Нет данных') }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h3>{{ __('Тип устройства') }}</h3>

                            @if(!$isPremium)
                                <div class="text-center">
                                    <a href="{{ route('about', ['#premium']) }}" target="_blank">
                                        <i class="fa fa-lock fa-2x"></i>
                                    </a>
                                </div>
                            @elseif($deviceTypes)
                                <canvas
                                        id="link-device-type-chart"
                                        chart-js-doughnut
                                        axis-x="{{ implode(';', array_keys($deviceTypes)) }}"
                                        axis-y="{{ implode(';', $deviceTypes) }}"
                                ></canvas>
                            @else
                                <p class="text-center text-black-50">
                                    {{ __('Нет данных') }}
                                </p>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h3>{{ __('Браузер') }}</h3>

                            @if(!$isPremium)
                                <div class="text-center">
                                    <a href="{{ route('about', ['#premium']) }}" target="_blank">
                                        <i class="fa fa-lock fa-2x"></i>
                                    </a>
                                </div>
                            @elseif($browsers)
                                <canvas
                                        id="link-browser-chart"
                                        chart-js-doughnut
                                        axis-x="{{ implode(';', array_keys($browsers)) }}"
                                        axis-y="{{ implode(';', $browsers) }}"
                                ></canvas>
                            @else
                                <p class="text-center text-black-50">
                                    {{ __('Нет данных') }}
                                </p>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h3>{{ __('Страна') }}</h3>

                            @if(!$isPremium)
                                <div class="text-center">
                                    <a href="{{ route('about', ['#premium']) }}" target="_blank">
                                        <i class="fa fa-lock fa-2x"></i>
                                    </a>
                                </div>
                            @elseif($countries)
                                <canvas
                                        id="link-country-chart"
                                        chart-js-doughnut
                                        axis-x="{{ implode(';', array_keys($countries)) }}"
                                        axis-y="{{ implode(';', $countries) }}"
                                ></canvas>
                            @else
                                <p class="text-center text-black-50">
                                    {{ __('Нет данных') }}
                                </p>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h3>{{ __('Город') }}</h3>

                            @if(!$isPremium)
                                <div class="text-center">
                                    <a href="{{ route('about', ['#premium']) }}" target="_blank">
                                        <i class="fa fa-lock fa-2x"></i>
                                    </a>
                                </div>
                            @elseif($cities)
                                <canvas
                                        id="link-city-chart"
                                        chart-js-doughnut
                                        axis-x="{{ implode(';', array_keys($cities)) }}"
                                        axis-y="{{ implode(';', $cities) }}"
                                ></canvas>
                            @else
                                <p class="text-center text-black-50">
                                    {{ __('Нет данных') }}
                                </p>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
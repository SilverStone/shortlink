@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        @include('link.form')

        <div class="alert {{ ($linksCount * 100 / config('common.user_links_limit')) > 90 ? 'alert-danger' : 'text-dark alert-secondary' }}">
            <small>
                {{ __('Количество активных ссылок') }}: {{ $linksCount }}.
                {{ __('Лимит активных ссылок') }}: {{ config('common.user_links_limit') }}.
                {{ __('В случае необходимости увеличения лимита ссылок свяжитесь с администратором') }}.
            </small>
        </div>

        @include('link.table')

    </div>
@endsection
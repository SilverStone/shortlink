<div class="row {{ $link->inProlongPeriod() ? 'bg-warning' : '' }}">
    <div class="col-md-8 col-sm-12">
        <p class="mb-0"><a href="{{ route('links.show', $link->key) }}">{{ $link->key }}</a></p>
        <small>{{ $link->target }}</small>
    </div>
    <div class="col-md-2 col-sm-12" link-prolong-value="{{ $link->id }}">
        {{ date('Y-m-d H:i', strtotime($link->expires_at)) }}
        @if($link->inProlongPeriod())
            <p class="mb-0">
                @if($isPremium)
                    <span class="btn btn-link p-0" link-prolong path="{{ route('links.prolong', $link->id) }}">
                        {{ __('Продлить') }}
                    </span>
                @else
                    <a href="{{ route('about', ['#premium']) }}" target="_blank">
                        {{ __('Продлить') }}
                    </a>
                @endif
            </p>
        @endif
    </div>
    <div class="col-md-2 col-sm-12 text-right">

        <i class="btn btn-link fa fa-copy p-0 pl-2" copy-to-clipboard="{{ route('redirect', $link->key) }}" title="{{ __('Скопировать в буфер') }}"></i>

        <a href="{{ route('redirect', $link->key) }}" target="_blank" class="btn btn-light text-primary fa fa-external-link p-0 pl-2" title="{{ __('Перейти') }}"></a>

        <a href="{{ route('links.show', $link->key) }}" target="_blank" class="btn btn-light text-primary fa fa-edit p-0 pl-2" title="{{ __('Редактировать') }}"></a>

        {{ Form::model($link, ['url' => route('links.disable', ['link' => $link->key, '_method' => 'POST']), 'delete-form' => '1']) }}
        <button type="submit" class="btn btn-light text-danger fa fa-times p-0 pl-2" title="{{ __('Удалить') }}"></button>
        {{ Form::close() }}
    </div>
</div>
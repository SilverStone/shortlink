<div class="row">
    <div class="col-md-9 col-sm-12">
        {{ $links->appends($_GET)->links('common.pagination') }}
    </div>
    <div class="col-md-3 col-sm-12">
        <div class="form-group">

            {{ Form::open([
                'id' => 'sorting-form',
                'method' => 'GET',
                'url' => route('links.index'),
            ]) }}

            {{ Form::select('sorting', $sortings, $sorting, [
            'class' => 'form-control',
             'sorting-field' => '1',
             'form' => 'sorting-form',
            ]) }}

            {{ Form::close() }}

        </div>
    </div>
</div>

<div class="grid-table">

    <div class="row head bg-dark text-white">
        <div class="col-md-8 col-sm-12">
            Ссылка
        </div>
        <div class="col-md-2 col-sm-12">
            Активна до
        </div>
        <div class="col-md-2 col-sm-12">
            Действия
        </div>
    </div>

    @forelse($links as $link)
        @include('link.item')
    @empty
        <div class="row">
            <div class="col-12 text-center">
                {{ __('Ссылки отсутствуют') }}
            </div>
        </div>
    @endforelse

</div>
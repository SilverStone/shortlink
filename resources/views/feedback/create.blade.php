@extends('layouts.modal')

@section('title')
    {{ __('Обратная связь') }}
@endsection

@section('body')

    {{ Form::open([
        'url' => route('feedback.store'),
        'method'=>'POST',
        'id' => 'modal-form',
        'modal-form' => 1,
    ]) }}

    <p>
        <small>
            Воспользоввшись формой обратной связи Вы можете задать вопрос, сообщить о недобросовестном использовании ресурса,
            ошибке. Так же Вы можете отправить предложение по развитию фунционала сервиса.
        </small>
    </p>

    <div class="form-group">
        <label>{{ __('E-Mail') }}:</label>
        {{ Form::text('email', (Auth::check() ? Auth::user()->email : null), ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>

    <div class="form-group">
        <label>{{ __('Текст сообщения') }}:</label>
        {{ Form::textarea('text', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
    </div>

    {{ Form::close() }}

@endsection

@section('footer')
    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Отмена') }}</button>
    <button type="submit" class="btn btn-primary" form="modal-form">{{ __('Отправить') }}</button>
@endsection
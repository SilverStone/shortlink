@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        <div class="row mb-3">
            <div class="col-md-3 col-sm-12">
                {{ __('E-Mail') }}: {{ $feedback->email }}
            </div>
            <div class="col-md-3 col-sm-12">
                {{ __('Пользователь') }}:
                @if($feedback->user)
                    <a href="{{ route('admin.users.show', $feedback->user->id) }}">
                        {{ $feedback->user->name }}
                    </a>
                @else
                    -
                @endif
            </div>
            <div class="col-md-3 col-sm-12">
                {{ __('Дата') }}: {{ date('Y-m-d H:i', strtotime($feedback->created_at)) }}
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        {!! nl2br($feedback->text) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
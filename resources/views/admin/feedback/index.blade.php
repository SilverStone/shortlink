@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        <div class="grid-table">

            <div class="row head bg-dark text-white">
                <div class="col-md-4 col-sm-12">
                    {{ __('E-Mail') }}
                </div>
                <div class="col-md-4 col-sm-12">
                    {{ __('Пользователь') }}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{ __('Дата') }}
                </div>
                <div class="col-md-2 col-sm-12">
                    {{ __('Действия') }}
                </div>
            </div>

            @forelse($feedback as $feedbackModel)

                <div class="row {{ $lastView < $feedbackModel->created_at ? 'bg-warning' : '' }}">
                    <div class="col-md-4 col-sm-12">
                        <a href="{{ route('admin.feedback.show', $feedbackModel->id) }}">
                            {{ $feedbackModel->email }}
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        @if($feedbackModel->user)
                            <a href="{{ route('admin.users.show', $feedbackModel->user->id) }}">
                                {{ $feedbackModel->user->name }}
                            </a>
                        @else
                            -
                        @endif
                    </div>
                    <div class="col-md-2 col-sm-12">
                        {{ date('Y-m-d H:i', strtotime($feedbackModel->created_at)) }}
                    </div>
                    <div class="col-md-2 col-sm-12 text-right">
                        {{ Form::model($feedbackModel, ['url' => route('admin.feedback.destroy', ['feedback' => $feedbackModel->id, '_method' => 'DELETE']), 'delete-form' => '1']) }}
                        <button type="submit" class="btn btn-light text-danger fa fa-times p-0 pl-2" title="{{ __('Удалить') }}"></button>
                        {{ Form::close() }}
                    </div>
                </div>

            @empty
                <div class="row">
                    <div class="col-12 text-center">
                        {{ __('Сообщения отсутствуют') }}
                    </div>
                </div>
            @endforelse

        </div>

    </div>
@endsection
@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        <div class="row">
            <div class="col-md-3 col-sm-12">
                <div class="form-group">

                    {{ Form::open([
                        'id' => 'sorting-form',
                        'method' => 'GET',
                        'url' => route('admin.domains.index'),
                    ]) }}

                    <label>Сортировка:</label>
                    {{ Form::select('sorting', $sortings, $sorting, [
                    'class' => 'form-control',
                     'sorting-field' => '1',
                     'form' => 'sorting-form',
                    ]) }}

                    {{ Form::close() }}

                </div>
            </div>
        </div>

        <table class="table" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Host</th>
                <th>Персональный лимит</th>
                <th>Количество ссылок</th>
            </tr>
            </thead>

            <tbody>
            @foreach($domains as $domain)
                <tr>
                    <td><a href="{{ route('admin.domains.edit', ['domain' => $domain->id]) }}">{{ $domain->host}}</a></td>
                    <td>{{ $domain->personal_limit }}</td>
                    <td>{{ $domain->links_count }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $domains->appends($_GET)->links() }}

    </div>
@endsection
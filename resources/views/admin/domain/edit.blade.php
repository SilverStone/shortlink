@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        {{ Form::model($domain, ['url' => route('admin.domains.update', ['domain' => $domain->id, '_method' => 'PATCH'])]) }}

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>Персональный лимит:</label>
                    {{ Form::text('personal_limit', null, ['class' => 'form-control'.($errors->has('personal_limit') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>Количество ссылок:</label>
                    {{ Form::text('links_count', $domain->links()->count(), ['class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 1]) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>

        {{ Form::close() }}

    </div>
@endsection
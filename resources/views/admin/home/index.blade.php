@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        <div class="row">
            <div class="col-md-8 col-sm-12">
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="card">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="{{ route('admin.users.index') }}">{{ __('Пользователи') }}</a>
                        </li>
                        <li class="list-group-item">
                            @php
                                $newFeedbackCount = \App\Helpers\UserHelper::newFeedbackCount(Auth::user());
                            @endphp
                            <a href="{{ route('admin.feedback.index') }}">{{ __('Обратная связь') }}
                                {!! ($newFeedbackCount ? '(<b>'.$newFeedbackCount.'</b>)' : '') !!}</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('admin.domains.index') }}">{{ __('Домены') }}</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('admin.banned-domains.index') }}">{{ __('Запрещенные домены') }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        <div class="row">
            <div class="col-md-3 col-sm-12">
                <div class="form-group">

                    {{ Form::open([
                        'id' => 'sorting-form',
                        'method' => 'GET',
                        'url' => route('admin.users.index'),
                    ]) }}

                    <label>Сортировка:</label>
                    {{ Form::select('sorting', $sortings, $sorting, [
                    'class' => 'form-control',
                     'sorting-field' => '1',
                     'form' => 'sorting-form',
                    ]) }}

                    {{ Form::close() }}

                </div>
            </div>
        </div>

        <table class="table" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Имя</th>
                <th>Ссылок</th>
                <th>Премиум до</th>
            </tr>
            </thead>

            <tbody>
            @foreach($users as $user)
                <tr>
                    <td><a href="{{ route('admin.users.show', ['$user' => $user->id]) }}">{{ $user->name}}</a></td>
                    <td>{{ $user->links()->count() }}</td>
                    <td>{{ $user->isPremium() ? date('Y-m-d H:i', strtotime($user->premium_until)) : ''}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $users->appends($_GET)->links() }}

    </div>
@endsection
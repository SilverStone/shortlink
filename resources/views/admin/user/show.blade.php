@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        <p>
            E-Mail: {{ $user->email }}
        </p>
        <p>
            Дата регистрации: {{ date('Y-m-d H:i', strtotime($user->created_at)) }}
        </p>
        <p>
            Премиум до: {{ $user->isPremium() ? date('Y-m-d H:i', strtotime($user->premium_until)) : ''}}
        </p>

        <h2>Доступ к премиум аккаунту</h2>

        {{ Form::open(['url' => route('admin.users.premium-add', ['user' => $user->id, '_method' => 'POST'])]) }}

        <div class="form-group">
            <label>Количество дней:</label>
            {{ Form::number('days', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Добавить</button>
        </div>

        {{ Form::close() }}

    </div>
@endsection
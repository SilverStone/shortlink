@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        @if($bannedDomain->id)
            {{ Form::model($bannedDomain, ['url' => route('admin.banned-domains.update', ['banned_domain' => $bannedDomain->id, '_method' => 'PATCH'])]) }}
        @else
            {{ Form::open(['url' => route('admin.banned-domains.store')]) }}
        @endif

        <div class="form-group">
            <label>Host:</label>
            {{ Form::text('host', null, ['class' => 'form-control'.($errors->has('host') ? ' is-invalid' : ''), 'autocomplete' => 'off']) }}
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>

        {{ Form::close() }}

    </div>
@endsection
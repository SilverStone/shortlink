@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        <p>
            <a class="btn btn-primary" href="{{ route('admin.banned-domains.create') }}">
                <span class="fa fa-plus"></span> Создать
            </a>
        </p>

        <table class="table" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Host</th>
                <th></th>
            </tr>
            </thead>

            <tbody>
            @foreach($bannedDomains as $bannedDomain)
                <tr>
                    <td>
                        <a href="{{ route('admin.banned-domains.edit', ['banned_domain' => $bannedDomain->id]) }}">
                            {{ $bannedDomain->host }}
                        </a>
                    </td>
                    <td>
                        {{ Form::model($bannedDomain, ['url' => route('admin.banned-domains.destroy', ['banned_domain' => $bannedDomain->id, '_method' => 'DELETE']), 'delete-form' => '1']) }}
                        <button type="submit" class="btn btn-danger fa fa-trash float-right"></button>
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $bannedDomains->render() }}

    </div>
@endsection
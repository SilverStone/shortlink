@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="centred">{{ $META_DATA['title'] }}</h1>

        <p class="text-center">
            <a href="{{ route('home') }}" class="btn btn-primary btn-lg">{{ __('Создать ссылку') }}</a>
        </p>

        <p class="text-center">
            Сервис по созданию коротких ссылок LetsGo предоставляет 3 уровня доступа для пользователя:
        </p>

        <div class="row">

            <div class="col-md-4 col-sm-12 mb-3">
                <div class="card">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item text-center bg-light">
                            <b>{{ __('Гость') }}</b>
                        </li>
                        <li class="list-group-item">
                            {{ __('Короткая ссылка от') }}:
                            <b>{{ config('common.link_length_guest') }}</b>
                            {{ __('символов') }}
                        </li>
                        <li class="list-group-item">
                            {{ __('Произвольный адрес') }}:
                            <b>{{ __('Нет') }}</b>
                        </li>
                        <li class="list-group-item">
                            {{ __('Активность ссылки') }}:
                            <b>{{ config('common.link_lifetime_guest') }}</b>
                            {{ __('дней') }}
                        </li>
                        <li class="list-group-item">
                            {{ __('Продление активности') }}:
                            <b>{{ __('Нет') }}</b>
                        </li>
                        <li class="list-group-item">
                            {{ __('Статистика') }}:
                            <b>{{ __('отсутствует') }}</b>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4 col-sm-12 mb-3">
                <div class="card">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item text-center bg-light">
                            <b>{{ __('Зарегистрированный') }}</b>
                        </li>
                        <li class="list-group-item">
                            {{ __('Короткая ссылка от') }}:
                            <b>{{ config('common.link_length_registered') }}</b>
                            {{ __('символов') }}
                        </li>
                        <li class="list-group-item">
                            {{ __('Произвольный адрес') }}:
                            <b>{{ __('Нет') }}</b>
                        </li>
                        <li class="list-group-item">
                            {{ __('Активность ссылки') }}:
                            <b>{{ config('common.link_lifetime_registered') }}</b>
                            {{ __('дней') }}
                        </li>
                        <li class="list-group-item">
                            {{ __('Продление активности') }}:
                            <b>{{ __('Нет') }}</b>
                        </li>
                        <li class="list-group-item">
                            {{ __('Статистика') }}:
                            <b>{{ __('базовая') }}</b>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4 col-sm-12 mb-3">
                <div class="card">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item text-center bg-light">
                            <b>{{ __('Премиум') }}</b>
                        </li>
                        <li class="list-group-item">
                            {{ __('Короткая ссылка от') }}:
                            <b>{{ config('common.link_length_premium') }}</b>
                            {{ __('символов') }}
                        </li>
                        <li class="list-group-item">
                            {{ __('Произвольный адрес') }}:
                            <b>{{ __('Да') }}</b>
                        </li>
                        <li class="list-group-item">
                            {{ __('Активность ссылки') }}:
                            <b>{{ config('common.link_lifetime_premium') }}</b>
                            {{ __('дней') }}
                        </li>
                        <li class="list-group-item">
                            {{ __('Продление активности') }}:
                            <b>{{ __('Да') }}</b>
                        </li>
                        <li class="list-group-item">
                            {{ __('Статистика') }}:
                            <b>{{ __('расширенная') }}</b>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

        <p>
            Более подробная информация по работе с сервисом создания коротких ссылок изложена в
            <a href="{{ route('rules') }}" target="_blank">правилах</a> использования сервиса.
        </p>

    </div>
@endsection
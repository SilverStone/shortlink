@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $META_DATA['title'] }}</h1>

        <h2>1. Общие положения</h2>

        <p>
            1.1. Правила использования сервиса (сайта) являются обязательными для исполнения всеми пользователями, в том числе и
            гостями.
        </p>

        <p>
            1.2. В случае нарушения данных правил администрация может заблокировать аккаунт пользователя без каких либо
            возвратов или компенсаций.
        </p>

        <p>
            1.3. Все данные, находящиеся на сервисе (сайте) являются собственностью администрации и могут удалятся и изменятся без
            дополнительных уведомлений.
        </p>

        <p>
            1.4. Администрация не несет ответственности за потерю данных или неработоспособность сервиса, вызыванную обстоятельствами непреодолимой силы,
            техническими сбоями в работе программ и оборудования, техническими работами, ограничениями доступа со стороны провадеров.
        </p>

        <h2>2. При использовании сервиса (сайта) запрещено</h2>

        <p>
            2.1. Запрещено использование сервиса (сайта) в мошенеческих целях, с целью распространения материалов нарушающих
            законодательство Российской Федерации.
        </p>

        <p>2.2. Запрещены любые действия нарушающие законодательство Российской Федерации.</p>

        <h2>3. Срок активности ссылки</h2>

        <p>3.1. Каждая ссылка обладает сроком активности, по истечении которого она становиться нерабочей.
            Вся связанная с ссылкой информация удаляется безвовзратно.</p>

        <p>3.2. Срок активности зависит от статуса пользователя и настраивается администратором.</p>

        <p>3.3. Обладатели премиум аккаунта могут продлевать срок активности ссылки за {{ config('common.link_prolong_period') }}
            дней до окончания текущего периода активности. Продление срока активности ссылки доступно многократно.</p>

        <p>3.4. Срок активности ссылки, созданной или продленной из премиум аккаунта не зависит от срока действия паремиум аккаунта.</p>

    </div>
@endsection
@extends('layouts.app')

@section('header-nav-class', 'mb-0')

@section('content')

    <div class="bg-light-blue pt-5 pb-5">
        <div class="container">
            <h1 class="centred">{{ $META_DATA['title'] }}</h1>
            @include('link.form')
        </div>
    </div>

    <div class="container text-center mt-5">

        <h2 class="centred">Больше возможностей</h2>

        <p class="important">
            Продление срока активности ссылок, статистика переходов, короткие ссылки от 4х символов
            и произвольный текст ссылок
        </p>
        <p>
            <a href="{{ route('about') }}" class="btn btn-warning btn-lg">{{ __('Подробнее') }}</a>
        </p>
    </div>

@endsection